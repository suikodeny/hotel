<?php

class Auth {

    function __construct(){}

    public function kill()
    {
        session_destroy();
        if (array_key_exists('user', $_SESSION))
                unset($_SESSION['user']);
    }

    public function __set($name, $value)
    {
        $_SESSION[$name] = serialize($value);
    }

    public function __get($name)
    {
        return array_key_exists($name, $_SESSION) ? unserialize($_SESSION[$name]) : false;
    }

}
