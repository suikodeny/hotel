<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 * Description of Msg
 * 
 * Classe para mostrar as msg de erro ou de informação.
 * É possível setar a msg e usá-la somente uma vez
 * 
 *
 * @author Atila
 */
class Msg {
    
    function __construct($msg, $tipo) {
        
        switch ($tipo) {
            case 1:
                $estilo = "OK";                
                break;
            case 2:
                $estilo = "WARNING";
                break;            
            case 3:
                $estilo = "ERROR";
                break;            
            default:
                $estilo = "INFO";
                break;
        }
        
        $msg = "<div class='".$estilo."'>$msg</div>";
        $_SESSION['FrameWorkMsg'] = $msg;
        
    }
    
    public static function getMsg(){
        if(isset($_SESSION['FrameWorkMsg'])){
            $m = $_SESSION['FrameWorkMsg'];
            $_SESSION['FrameWorkMsg'] = null;
            return $m;
        }
        return false;
    }
}

?>
