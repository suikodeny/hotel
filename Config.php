<?php

/*
 * Padrão de projeto - ACTIVE RECORD
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 
 * Description of Config
 * 
 * Arquivo de configuração para setar qual DataBase será utilizada e seus parâmetros como
 * localhost, usuario do banco, senha do usuario do banco, nome do banco default,etc
 *  
 * @author Atila Alves Pires
 */

date_default_timezone_set("America/Campo_Grande");
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "123");
define("DB_NAME", "hotel");
define("WEB_ROOT", "index");

?>