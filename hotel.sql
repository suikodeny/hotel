-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 03-Ago-2015 às 23:10
-- Versão do servidor: 5.6.25-0ubuntu0.15.04.1
-- PHP Version: 5.6.4-4ubuntu6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hotel`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `apartamento`
--

CREATE TABLE IF NOT EXISTS `apartamento` (
  `idApartamento` int(10) NOT NULL,
  `andarApartamento` int(11) NOT NULL,
  `tvApartamento` tinyint(1) NOT NULL,
  `arApartamento` tinyint(1) NOT NULL,
  `reservadoApartamento` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `apartamento`
--

INSERT INTO `apartamento` (`idApartamento`, `andarApartamento`, `tvApartamento`, `arApartamento`, `reservadoApartamento`) VALUES
(101, 1, 1, 1, 0),
(102, 1, 1, 1, 0),
(201, 2, 1, 1, 0),
(202, 2, 1, 0, 0),
(301, 3, 1, 1, 0),
(302, 3, 1, 1, 0),
(401, 4, 0, 1, 0),
(402, 4, 0, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
`idCliente` int(10) unsigned NOT NULL,
  `nomeCliente` char(50) COLLATE latin1_general_ci NOT NULL,
  `sobrenomeCliente` char(150) COLLATE latin1_general_ci NOT NULL,
  `cpfCliente` char(11) COLLATE latin1_general_ci NOT NULL,
  `sexoCliente` char(1) COLLATE latin1_general_ci NOT NULL,
  `emailCliente` char(40) CHARACTER SET utf8 NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nomeCliente`, `sobrenomeCliente`, `cpfCliente`, `sexoCliente`, `emailCliente`, `ativo`) VALUES
(1, 'Atila', 'Alves Pires', '12345678910', 'M', 'suikodeny@gmail.com', 1),
(11, 'francisco', 'neto', '12345678910', 'M', 'francisco.neto@gmail.com', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`idLogin` int(10) unsigned NOT NULL,
  `usuarioLogin` char(50) COLLATE latin1_general_ci NOT NULL,
  `senhaLogin` char(255) COLLATE latin1_general_ci NOT NULL,
  `tentativas` int(1) NOT NULL DEFAULT '0',
  `ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Extraindo dados da tabela `login`
--

INSERT INTO `login` (`idLogin`, `usuarioLogin`, `senhaLogin`, `tentativas`, `ativo`) VALUES
(1, 'atila.pires', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 0, 1),
(2, 'fabio.lopes', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', 0, 1),
(5, 'francisco.neto@gmail.com', 'fa585d89c851dd338a70dcf535aa2a92fee7836dd6aff1226583e88e0996293f16bc009c652826e0fc5c706695a03cddce372f139eff4d13959da6f1f5d3eabe', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `loginLogs`
--

CREATE TABLE IF NOT EXISTS `loginLogs` (
  `idLoginLogs` int(10) unsigned NOT NULL,
  `dataHora` int(10) NOT NULL,
  `sucesso` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `loginLogs`
--

INSERT INTO `loginLogs` (`idLoginLogs`, `dataHora`, `sucesso`) VALUES
(1, 1438113764, 1),
(1, 1438121946, 1),
(1, 1438122023, 0),
(1, 1438123752, 0),
(1, 1438123826, 1),
(1, 1438196693, 1),
(2, 1438196705, 0),
(2, 1438196717, 1),
(1, 1438197403, 1),
(1, 1438197498, 1),
(1, 1438197515, 1),
(1, 1438197525, 1),
(1, 1438199753, 1),
(1, 1438462831, 1),
(1, 1438462866, 1),
(1, 1438462901, 0),
(1, 1438462926, 0),
(1, 1438463223, 1),
(1, 1438469060, 1),
(1, 1438469146, 1),
(1, 1438517201, 1),
(1, 1438517720, 1),
(1, 1438517803, 1),
(1, 1438518477, 1),
(1, 1438524647, 1),
(1, 1438525557, 1),
(1, 1438527129, 1),
(3, 1438562366, 1),
(1, 1438564168, 1),
(1, 1438650566, 1),
(1, 1438651710, 1),
(1, 1438652999, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `reserva`
--

CREATE TABLE IF NOT EXISTS `reserva` (
`idReserva` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `idApartamento` int(11) NOT NULL,
  `dataReserva` int(11) NOT NULL,
  `aberta` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apartamento`
--
ALTER TABLE `apartamento`
 ADD PRIMARY KEY (`idApartamento`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
 ADD PRIMARY KEY (`idCliente`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`idLogin`);

--
-- Indexes for table `loginLogs`
--
ALTER TABLE `loginLogs`
 ADD PRIMARY KEY (`dataHora`);

--
-- Indexes for table `reserva`
--
ALTER TABLE `reserva`
 ADD PRIMARY KEY (`idReserva`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
MODIFY `idCliente` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
MODIFY `idLogin` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `reserva`
--
ALTER TABLE `reserva`
MODIFY `idReserva` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
