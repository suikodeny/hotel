<?php

/*
 * Padrão de projeto - ACTIVE RECORD
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 * Description of DB
 * 
 * Classe que possui os métodos que serão utilizados para para interagir com 
 * banco de dados e utiliza o arquivo de config.php com os parametros para 
 * conectar a banco de dados. 
 * 
 * @author Atila
 */

abstract class Db {

    protected $host = DB_HOST;
    protected $user = DB_USER;
    protected $pass = DB_PASS;
    protected $dbname = DB_NAME;
    protected $dbh; // database handler
    protected $stmt; // statement
    protected $error;
    protected $query = "";

    abstract protected function conectar();

    public function query($query, $filtro=NULL) {
        $this->query = $query;
        $this->stmt = $this->dbh->prepare($query);
    }

    public function bind($param, $value, $type = null) {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    public function execute() {
        try{
        return $this->stmt->execute();
        }
        catch(PDOException $p){
               $this->error = $p->getMessage();
               echo $this->error;
        }
    }

    public function getResults($class = NULL) {
        $this->execute();
        if ($class)
            return $this->stmt->fetchAll(PDO::FETCH_CLASS, $class);
        return $this->stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function getRow($class = NULL) {
        if ($class) {
            $this->stmt->setFetchMode(PDO::FETCH_CLASS, $class);
            $this->execute();
            return $this->stmt->fetch(PDO::FETCH_CLASS);
        }
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }

    public function rowCount() {
        return $this->stmt->rowCount();
    }

    public function lastInsertId() {
        return $this->dbh->lastInsertId();
    }

    public function beginTransaction() {
        return $this->dbh->beginTransaction();
    }

    public function endTransaction() {
        return $this->dbh->commit();
    }

    public function cancelTransaction() {
        return $this->dbh->rollBack();
    }

    public function debugDumpParams() {
        return $this->stmt->debugDumpParams();
    }

    public function getError() {
        return $this->error;
    }
}

// fim da classe



