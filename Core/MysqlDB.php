<?php

/*
 * Padrão de projeto - ACTIVE RECORD
 * 
 * classe que extende a classe DB para implementar o metodo conectar
 * devolve o nome do erro caso de algo errado 
 * 
 * @author Atila
 */


final class MysqlDB extends Db {

    function __construct() {
        $this->conectar();
    }

    protected function conectar() {
        // Database Source Name
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
        // Set options
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
        );
        // Create a new PDO instanace
        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        }
        // Catch any errors
        catch (PDOException $e) {
            $this->error = $e->getMessage();
            echo $this->error;
        }
    }

}

