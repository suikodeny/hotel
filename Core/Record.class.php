<?php

/*
 * Padrão de projeto - ACTIVE RECORD
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Description of Record
 *
 * Classe Record possui o metodo save para salvar direto no bando de dados 
 * Utiliza ums instancia da classe MysqlDB
 * Monta uma query direto numa String de forma mais genérica possível, mapeando os atributos da classe
 * 
 * @author Atila
 */

class Record {

    public function save() {
        $db = new MysqlDB();
        $table = $this::TABLE;
        $pk = $this::PK;
        $atts = array(); //atributos
        //query do save
        if (empty($this->$pk)) {
            foreach ($this as $key => $value) {
                if (is_array($value) || is_object($value) || is_null($value))
                    continue;
                $atts[$key] = $value;
            }

            //para montar o insert
            $q = "INSERT INTO $table (";

            //array_keys serve para pegar somente os nomes das chaves do array
            $q .= implode(",", array_keys($atts)) . ") ";
            $q .= "VALUES (:";
            $q .= implode(",:", array_keys($atts)) . ")";
        }else {
            //query do update
            foreach ($this as $key => $value) {
                if (is_array($value) || is_object($value))
                    continue;
                $atts[$key] = $value;
                $fields[] = $key . '=:' . $key;
                $q = "UPDATE $table SET " . implode(',', $fields);
                $q .= " WHERE $pk =:" . $pk;
            }
        }

        $db->query($q);
        foreach ($atts as $key => $value) {
            $db->bind(":" . $key, $value);
        }

        return $db->execute();
    }

//fim do save

    /**
     * A função 'saveId()' serve para salvar tuplas na tabela do banco de dados que não possua uma
     * chave primária com atributo de 'auto_increment'.
     * 
     * @return boolean Retorna a confirmação da gravação da linha.
     */
    public function saveId() {
        $db = new MysqlDB();
        $table = $this::TABLE;
        $pk = $this::PK;
        $atts = array(); //atributos
        //query do save
        if (empty($this->$pk)) {
            foreach ($this as $key => $value) {
                if (is_array($value) || is_object($value) || is_null($value))
                    continue;
                $atts[$key] = $value;
            }

            //para montar o insert
            $q = "INSERT INTO $table (";

            //array_keys serve para pegar somente os nomes das chaves do array
            $q .= implode(",", array_keys($atts)) . ") ";
            $q .= "VALUES (:";
            $q .= implode(",:", array_keys($atts)) . ")";
            //echo '--------insert id------------  ';
            //echo $q;
        }

        $db->query($q);
        foreach ($atts as $key => $value) {
            $db->bind(":" . $key, $value);
        }
        return $db->execute();
    }

    public function load($id = null) {
        $db = new MysqlDB();
        $pk = $this::PK;
        $table = $this::TABLE;

        if (empty($id))
            return false;

        //para montar o insert
        $q = "SELECT * FROM $table WHERE $pk =:" . $pk;
        $db->query($q);
        $db->bind(':' . $pk, $id);

        //pega somente uma linha e gera um objeto de quem chamou
        $data = $db->getRow(get_class($this));

        //verifica se o objeto $data não está vazio
        if (empty($data)) {
            return false;
        }

        foreach ($data as $key => $value) {
            if (empty($this->$key)) {
                $this->$key = $data->$key;
            }
        }
    }

    public function delete($id = null) {
        $db = new MysqlDB();
        $pk = $this::PK;
        $table = $this::TABLE;

        if (empty($id))
            $id = $this->$pk;

        $q = "DELETE FROM $table WHERE $pk =:" . $pk;
        $db->query($q);
        $db->bind(':' . $pk, $id);
        return $db->execute();
    }

    public static function getList($criteria = NULL) {
        $db = new MysqlDB();

        //descobre quem esta chamando o metodo getList(). Qual a classe do objeto
        $class = get_called_class();
        $table = $class::TABLE;

        //sem passar criterios Criteria
        $q = "SELECT * FROM $table ";
        if (empty($criteria)) {
            $db->query($q);
            return $db->getResults($class);
        }

        //passando o objeto Criteria
        if ($criteria->getConditions()) {
            $conditions = array();
            foreach ($criteria->getConditions() as $c) {
                $conditions[] = $c[0] . ' ' . $c[1] . ' :' . $c[0];
            }
            $q .= ' WHERE ' . implode(' AND ', $conditions);
        }
        if ($criteria->getOrder())
            $q .= ' ORDER BY ' . $criteria->getOrder();

        if ($criteria->getLimit())
            $q .= ' LIMIT ' . $criteria->getLimit();

        $db->query($q);
        foreach ($criteria->getConditions() as $c) {
            $db->bind(':' . $c[0], $c[2]);
        }
        return $db->getResults($class);
    }

}

?>
