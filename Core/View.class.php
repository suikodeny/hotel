<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 * 
 * Description of View
 *
 * @author Miguel
 */
class View {

    private $vars = array();
    private $module;
    private $view;
    private $template;
    private $_title;

    public function __construct($module, $view, $template=NULL) {
        $this->module = $module;
        $this->view = $view;
        $this->template = $template;        
        if(isset($_GET['ajax'])){
            $this->template = null;   
        }            
    }

    public function __set($name, $value) {
        $this->vars[$name] = $value;
    }

    public function setTitle($t) {
        $this->_title = $t;
    }

    public function render() {
        header("Content-Type: text/html; charset=UTF-8", true);
        foreach ($this->vars as $key => $value) {
            $$key = $value;
        }
        $view = 'View/' . ucfirst($this->module) . '/' . ucfirst($this->view) . '.php';
        $title = $this->_title;
        if (empty($this->template))
            include $view;
        else
            include 'Template/' . $this->template . '/index.php';
    }

}

?>
