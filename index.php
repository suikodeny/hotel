<?php

session_start();

//Todos os includes Core / Model / Controller além de Config.php 
//Core
include_once 'Core/Db.php';
include_once 'Core/MysqlDB.php';
include_once 'Core/Criteria.class.php';
include_once 'Core/Record.class.php';
include_once 'Core/View.class.php';

//Model
include_once 'Model/Reserva.php';
include_once 'Model/Login.php';
include_once 'Model/Cliente.php';
include_once 'Model/Apartamento.php';
include_once 'Model/LoginLogs.php';

//Controller
include_once 'Controller/LoginController.php';
include_once 'Controller/ApartamentoController.php';
include_once 'Controller/ClienteController.php';
include_once 'Controller/ReservaController.php';

include_once 'Helpers/Auth.php';
include_once 'Helpers/Msg.class.php';

//Config.php
include_once 'Config.php';

$controller = strip_tags(@$_GET["m"]);
$action = strip_tags(@$_GET["a"]);

if (empty($controller)) {
    $controller = "login";
    $action = "login";
}
//poe a string tudo para minusculo
$controller = strtolower($controller);
$action = strtolower($action);

//Somente a primeira letra é maiuscula
$controller = ucfirst($controller);

//esta instanciando a classe controler passando como referencia a variável
$controller = $controller . "Controller";
$c = new $controller();
$c->$action();    

?>
