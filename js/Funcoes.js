/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(
    function(){
        $('#naveg ul li').hover(function(){
            $(this).find('ul li').slideDown();
        }, function(){
            $(this).find('ul li').slideUp()
        });
        
        $('#busca').keyup(function(){
            if($('#busca').val().length > 2){            
                $.ajax({
                    url:'?ajax=true&m='+ $('#m').val() +'&a='+ $('#a').val() +'&busca=' + $('#busca').val(),
                    type:'get',
                    success:function(data){
                    
                        $('#lista').html(data);                    
                        $('#lista #header_section').remove();                                           
                    },
                    error:function(){
                        alert('Falha ao Buscar')
                    }
                });
            }
        });
    });
    
    function mostrar(){
        $(".parag_add_treino").toggle('slow');
    }