<?php

/**
 * Model Cliente. Serve para pegar os dados do cliente e buscar um array de reservas.
 *
 * @author Atila Alves Pires
 * 
 */
class Cliente extends Record {

    protected $idCliente;
    protected $nomeCliente;
    protected $sobrenomeCliente;
    protected $cpfCliente;
    protected $emailCliente;
    protected $sexoCliente;
    protected $ativo;
    protected $perfil;
    private $login;
    private $reservas = array();

    const TABLE = "cliente";
    const PK = "idCliente";

    function __construct($id = null) {
        if (!empty($id)) {
            $this->load($id);
        }
    }

    public function getIdCliente() {
        return $this->idCliente;
    }

    public function setIdCliente($idCliente) {
        $this->idCliente = $idCliente;
    }

    public function getNomeCliente() {
        return $this->nomeCliente;
    }

    public function setNomeCliente($nomeCliente) {
        $this->nomeCliente = $nomeCliente;
    }

    public function getSobrenomeCliente() {
        return $this->sobrenomeCliente;
    }

    public function setSobrenomeCliente($sobrenomeCliente) {
        $this->sobrenomeCliente = $sobrenomeCliente;
    }

    public function getCpfCliente() {
        return $this->cpfCliente;
    }

    public function setCpfCliente($cpfCliente) {
        $this->cpfCliente = $cpfCliente;
    }

    public function getEmailCliente() {
        return $this->emailCliente;
    }

    public function setEmailCliente($emailCliente) {
        $this->emailCliente = $emailCliente;
    }

    public function getSexoCliente() {
        return $this->sexoCliente;
    }

    public function setSexoCliente($sexoCliente) {
        $this->sexoCliente = $sexoCliente;
    }

    /**
     * @return Array Retorna um array de reservas feitas para o usuário
     */
    public function getReservas() {
        if (empty($this->reservas)) {
            $criterio = new Criteria();
            $criterio->addCondition("idCliente", "=", $this->idCliente);
            $this->reservas = Reserva::getList($criterio);
        }
        return $this->reservas;
    }

    /**
     * Setar as reservas feitas para o usuário. 
     * 
     * @param Array $reservas = Segue o modelo /Model/Reserva.php
     */
    public function setReservas($reservas) {
        $this->reservas = $reservas;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    function getPerfil() {
        return $this->perfil;
    }

    function setPerfil($perfil) {
        $this->perfil = $perfil;
    }

    function getLogin() {
        if (count($this->login) == 0) {

            $criterio = new Criteria();
            $criterio->addCondition("usuarioLogin", "=", $this->emailCliente);

            $lista = Login::getList($criterio);

            $this->login = new Login($lista[0]->getIdLogin());
        }
        return $this->login;
    }

}

?>
