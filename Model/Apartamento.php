<?php
/**
 * Model Apartamento. Lista de apartamentos do hotel
 * 
 * @author Atila Alves Pires
 */

class Apartamento extends Record{
    
    protected $idApartamento;
    protected $andarApartamento;
    protected $tvApartamento;
    protected $arApartamento;
    protected $reservadoApartamento;
   
    private $reservas = array();
    
    const TABLE = "apartamento";
    const PK = "idApartamento";
    
    function __construct($id = null) {
        if(!empty($id)){
            $this->load($id);
        }   
    }
    
    function getIdApartamento() {
        return $this->idApartamento;
    }

    function getAndarApartamento() {
        return $this->andarApartamento;
    }

    function getTvApartamento() {
        return $this->tvApartamento;
    }

    function getArApartamento() {
        return $this->arApartamento;
    }

    function getReservadoApartamento() {
        return $this->reservadoApartamento;
    }

    function setIdApartamento($idApartamento) {
        $this->idApartamento = $idApartamento;
    }

    function setAndarApartamento($andarApartamento) {
        $this->andarApartamento = $andarApartamento;
    }

    function setTvApartamento($tvApartamento) {
        $this->tvApartamento = $tvApartamento;
    }

    function setArApartamento($arApartamento) {
        $this->arApartamento = $arApartamento;
    }

    function setReservadoApartamento($reservadoApartamento) {
        $this->reservadoApartamento = $reservadoApartamento;
    }
    
    // para pegar as resvas
    public function getReservas() {
        if(empty($this->reservas)){
            $criterio = new Criteria();            
            $criterio->addCondition('idApartamento', '=', $this->getIdApartamento());
            $this->reservas = Reserva::getList($criterio);
        }
        return $this->reservas;        
    }
    
    //para sertar as reservas
    public function setReservas($reservas) {
        $this->reservas = $reservas;
    }
}

?>
