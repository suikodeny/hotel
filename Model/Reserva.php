<?php
/**
 * Model Reserva. Classe para guardar as reservas do Hotel.
 * Nenhuma reserva será cadastrado sem a o cliente
 *
 * @author Atila Alves Pires
 */

class Reserva extends Record{
    
    protected $idReserva;
    protected $idCliente;
    protected $idApartamento;
    protected $dataReserva;
    protected $aberta;
           
    const TABLE = "reserva";
    const PK = "idReserva";
    
    function __construct($id = null) {
        if(!empty($id)){
            $this->load($id);
        }   
    }
    
    function getIdReserva() {
        return $this->idReserva;
    }

    function getIdCliente() {
        return $this->idCliente;
    }

    function getIdApartamento() {
        return $this->idApartamento;
    }

    function getDataReserva() {
        return $this->dataReserva;
    }
    
    function getAberta() {
        return $this->aberta;
    }

    function setIdReserva($idReserva) {
        $this->idReserva = $idReserva;
    }

    function setIdCliente($idCliente) {
        $this->idCliente = $idCliente;
    }

    function setIdApartamento($idApartamento) {
        $this->idApartamento = $idApartamento;
    }

    function setDataReserva($dataReserva) {
        $this->dataReserva = $dataReserva;
    }
    
    function setAberta($aberta) {
        $this->aberta = $aberta;
    }

}
?>
