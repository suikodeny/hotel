<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 * Description of Login
 *
 * @author Atila
 */
class Login extends Record{
    protected $idLogin;
    protected $usuarioLogin;
    protected $senhaLogin;
    protected $tentativas;
    protected $ativo;


    private $cliente;
    
    const TABLE = "login";
    const PK = "idLogin";
    
    function __construct($id = null) {
        if(!empty($id)){
            $this->load($id);
        }   
    }
    
    public function getIdLogin() {
        return $this->idLogin;
    }

    public function setIdLogin($idLogin) {
        $this->idLogin = strip_tags($idLogin);
    }

    public function getUsuarioLogin() {
        return $this->usuarioLogin;
    }

    public function setUsuarioLogin($usuarioLogin) {
        $this->usuarioLogin = strip_tags($usuarioLogin);
    }

    public function getSenhaLogin() {
        return $this->senhaLogin;
    }

    public function setSenhaLogin($senhaLogin) {
        $this->senhaLogin = strip_tags($senhaLogin);
    }
        
    public function getCliente() {
        if (count($this->cliente) == 0) {
            $criterio = new Criteria();
            $criterio->addCondition("emailCliente", "=", $this->getUsuarioLogin());
            
            $lista = Cliente::getList($criterio);
            
            $this->cliente = new Cliente($lista[0]->getIdCliente());
        }
        return $this->cliente;
    }  
    
    public function getTentativas() {
        return $this->tentativas;
    }

    public function setTentativas($tentativas) {
        $this->tentativas = strip_tags($tentativas);
    }
    
    function getAtivo() {
        return $this->ativo;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

}

?>
