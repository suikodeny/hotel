<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 * Description of Login
 *
 * @author Atila
 */
class LoginLogs extends Record{
    protected $idLoginLogs;
    protected $dataHora;
    protected $sucesso;
           
    const TABLE = "loginLogs";
    const PK = "";
    
    function __construct($id = null) {
        if(!empty($id)){
            $this->load($id);
        }   
    }
    
    function getIdLoginLogs() {
        return $this->idLoginLogs;
    }

    function getDataHora() {
        return $this->dataHora;
    }

    function getSucesso() {
        return $this->sucesso;
    }

    function setIdLoginLogs($idLoginLogs) {
        $this->idLoginLogs = strip_tags($idLoginLogs);
    }

    function setDataHora($dataHora) {
        $this->dataHora = strip_tags($dataHora);
    }

    function setSucesso($sucesso) {
        $this->sucesso = strip_tags($sucesso);
    }

}

?>
