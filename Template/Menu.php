<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 * Description of menu
 *
 * @author Atila
 */
?>

<ul>
    <?php
    $auth = new Auth();
    if (!$auth->user) {
        echo '<li><a href="?m=login&a=login">ALATJUNEPERO</a></li>';
    } else if ($auth->user->getPerfil() == 1) {
        echo '<li><a href="?m=cliente&a=all">Clientes</a>';
        echo '<ul><li><a href="?m=cliente&a=add">Novo</a></li></ul>';
        echo '</li>';
        echo '<li><a href="?m=apartamento&a=all">Apartamento</a></li>';
        echo '<li><a href="?m=reserva&a=add">Reserva</a></li>';
        echo '<li><a href="?m=login&a=all">Usuários</a></li>';
        echo '<li><a href="?m=login&a=logout">Logout</a></li>';
    } else {
        echo '<li><a href="?m=cliente&a=all">Clientes</a></li>';
        echo '<li><a href="?m=reserva&a=add">Reserva</a></li>';
        echo '<li><a href="?m=login&a=logout">Logout</a></li>';
    }
    ?>
</ul>
