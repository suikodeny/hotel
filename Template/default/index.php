<?php !isset($this) ? exit : true; ?>

<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title; ?></title>
        
        <link rel="stylesheet" type="text/css" href="Template/default/estilo.css">		
        <script type="text/javascript" src="./js/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="./js/Funcoes.js"></script>
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $("#dataReserva").datepicker();
            });            
        </script>

    </head>	
    <body>

        <nav id="naveg">

            <?php
            include "Template/Menu.php";
            ?> 			

        </nav>
        <section id="conteudo">	            
            <?php
            echo Msg::getMsg();
            include $view;
            ?>
        </section>
        <footer id="rodape">	
            <h6>Todos os direitos reservados - Criado em PHP por Suikodeny </h6>
        </footer>
    </body>
</html>