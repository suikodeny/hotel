<?php

class LoginController {

    public function login() {
        if (count($_POST) > 0) {
            $login = strip_tags($_POST['login']);
            $senha = strip_tags($_POST['senha']);

            $criterio = new Criteria();
            $criterio->addCondition("usuarioLogin", "=", $login);
            //retorna a lista de usuários com o login informada anteriormente
            $usuario = Login::getList($criterio);

            if (count($usuario) > 0) {
                $usuario = new Login(intval($usuario[0]->getIdLogin()));
                $loginLog = new LoginLogs();
                $loginLog->setIdLoginLogs($usuario->getIdLogin());
                $loginLog->setDataHora(time());

                if ($usuario->getTentativas() >= 2) {
                    new Msg("Usuário Bloqueado por erro de senha", 2);
                    $usuario->setTentativas($usuario->getTentativas() + 1);
                    $usuario->save();
                } else {
                    if ($usuario->getSenhaLogin() == hash("sha512", $senha)) {
                        $auth = new Auth();
                        $auth->user = $usuario->getCliente();
                        new Msg("Seja Bem Vindo, " . $auth->user->getNomeCliente() . " !", 1);

                        $loginLog->setSucesso(TRUE);
                        $loginLog->saveId();

                        $f = new ClienteController();
                        $f->all();
                        return;
                    } else {
                        new Msg("Login ou senha Incorretos", 2);

                        $loginLog->setSucesso(FALSE);
                        $loginLog->saveId();
                    }
                }
            } else {
                new Msg("Não Cadastrado", 2);
            }
        }
        $view = new View('login', 'login', 'default');
        $view->setTitle("Login");
        $view->render();
    }

    public function logout() {
        $auth = new Auth();
        $auth->kill();

        $this->login();
    }

    public function all() {

        $auth = new Auth();

        if (!$auth->user || $auth->user->getPerfil() == 5) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $view = new View('login', 'all', 'default');
        $criteria = new Criteria();
        $criteria->addOrder("idLogin");
        $criteria->addCondition('ativo', '=', TRUE);
        $view->usuarios = Login::getList($criteria);
        $view->setTitle("Lista de Usuario");
        $view->render();
    }

    public function view() {

        $auth = new Auth();

        if (!$auth->user || $auth->user->getPerfil() == 5) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $id = strip_tags(@$_GET['id']);
        $view = new View('login', 'view', 'default');
        $view->user = new Login($id);
        $view->setTitle("Usuário");
        $view->render();
    }

    public function add() {

        $auth = new Auth();

        if (!$auth->user || $auth->user->getPerfil() == 5) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $login = new Login();
        $view = new View("login", "add", "default");
        $view->setTitle("Cadastro de Usuário");

        if (count($_POST) > 0) {
            if (strip_tags(@$_POST['senha_usuario']) == strip_tags(@$_POST['repete_senha_usuario'])) {
                $login->setLogin(strip_tags(@$_POST['login_usuario']));
                $login->setSenha(hash("sha512", strip_tags(@$_POST['senha_usuario'])));
                if ($login->save()) {
                    $criterio = new Criteria();
                    $criterio->addOrder("login DESC");
                    $criterio->addLimit(1);
                    $login = Login::getList($criterio);

                    //$usuario->setId_usuario($login[0]->getId_login());
                    $usuario = new Usuario();
                    $usuario->setNome_usuario(strip_tags(@$_POST['nome_usuario']));
                    $usuario->setSobrenome_usuario(strip_tags(@$_POST['sobrenome_usuario']));
                    $usuario->setEmail(strip_tags(@$_POST['email_usuario']));

                    if ($usuario->save()) {
                        new Msg("Registrado com sucesso", 1);
                    }
                } else {
                    new Msg("Erro de registro de usuário!", 2);
                }
            } else {
                new Msg("Os campos de senha não coincidem", 2);
            }
        }
        $view->login = $login;
        $view->render();
    }

    public function edit() {

        $auth = new Auth();

        if (!$auth->user || $auth->user->getPerfil() == 5) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $id = @$_GET['id'];
        $login = new Login($id);
        $view = new View("login", "edit", "default");
        $view->user = $login;
        $view->setTitle("Edição de Usuário");

        if (count($_POST) > 0) {
            if (strip_tags(@$_POST['senha_usuario']) == strip_tags(@$_POST['repete_senha_usuario'])) {
                if (trim(strip_tags(@$_POST['repete_senha_usuario'])) <> "") {
                    $login->setSenhaLogin(hash("sha512", strip_tags(@$_POST['senha_usuario'])));
                }
                $criterio = new Criteria();
                $criterio->addCondition("emailCliente", "=", $login->getUsuarioLogin());

                $listaCliente = Cliente::getList($criterio);
                $cliente = new Cliente(intval($listaCliente[0]->getIdCliente()));
                $cliente->setPerfil(strip_tags(@$_POST['codPerfil']));

                if ($login->save()) {
                    $cliente->save();
                    new Msg("Editado com sucesso", 1);
                } else {
                    new Msg("Erro de edição de usuário!", 2);
                }
            } else {
                new Msg("Os campos de senha não coincidem", 2);
            }
        }
        $view->render();
    }

    public function delete() {

        $auth = new Auth();

        if (!$auth->user || $auth->user->getPerfil() == 5) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $id = strip_tags(@$_GET['id']);
        $login = new Login($id);
        $cliente = $login->getCliente();

        $cliente->setAtivo(FALSE);
        $login->setAtivo(FALSE);

        if ($cliente->save()) {
            $login->save();
        }

        $this->all();
        return;
    }

}
