<?php

/**
 * Description of ReservaController
 *
 * @author Atila Alves Pires
 */
class ReservaController {

    public function add() {

        $auth = new Auth();

        if (!$auth->user) {
            die('acesso negado');
        }

        $reserva = new Reserva();
        $view = new View("reserva", "add", "default");
        $view->setTitle("Cadastro de Reserva");

        if (count($_POST) > 0) {
            if (strtotime(str_replace("/", "-", strip_tags(@$_POST['dataReserva']))) > strtotime(date('dmy'))) {
                $reserva->setDataReserva(strtotime(str_replace("/", "-", strip_tags(@$_POST['dataReserva']))));
                $reserva->setIdApartamento(strip_tags(@$_POST['idApartamento']));
                $reserva->setIdCliente(strip_tags(@$_POST['idCliente']));

                $apartamento = new Apartamento(strip_tags(@$_POST['idApartamento']));
                $apartamento->setReservadoApartamento(TRUE);

                if ($reserva->save()) {
                    $msg = "Registrado com sucesso!";
                    $apartamento->save();
                    ApartamentoController::all();
                    return;
                }
            } else {
                $msg = "Erro de Registro! Verifique o preenchimento dos campos";
                new Msg($msg, 2);
            }
        }

        $view->reserva = $reserva;
        $view->user = $auth->user;
        $view->render();
    }

    public function edit() {

        $auth = new Auth();

        if (!$auth->user) {
            die('acesso negado');
        }

        $id = strip_tags(@$_GET['id']);
        $reserva = new Reserva($id);
        $view = new View("reserva", "edit", "default");
        $view->reserva = $reserva;
        $view->setTitle("Edição de Reserva");

        if (count($_POST) > 0) {
            $reserva->setDataReserva(strtotime(str_replace("/", "-", strip_tags(@$_POST['dataReserva']))));

            $reserva->setIdApartamento(strip_tags(@$_POST['idApartamento']));
            $reserva->setIdCliente(strip_tags(@$_POST['idCliente']));

            if ($reserva->save()) {
                $msg = "Editado com sucesso!";
                $this->all();
            } else {
                $msg = "Erro de Registro! Verifique o preenchimento dos campos";
                new Msg($msg, 2);
            }
        }
        $view->render();
    }

    public function delete() {

        $auth = new Auth();

        if (!$auth->user) {
            die('acesso negado');
        }

        $id = strip_tags(@$_GET['id']);
        $reserva = new Reserva($id);
        $apartamento = new Apartamento(strip_tags($reserva->getIdApartamento()));
        $apartamento->setReservadoApartamento(FALSE);
        $apartamento->save();
        $reserva->delete();

        ApartamentoController::all();
        return;
    }

}

?>
