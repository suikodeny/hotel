<?php

/**
 * Description of Apartamento
 *
 * @author Atila Alves Pires
 */
class ApartamentoController {

    public function all() {

        $auth = new Auth();
        if (!$auth->user || $auth->user->getPerfil() == 5) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $criterio = new Criteria();
        $criterio->addOrder("idApartamento");

        if (isset($_GET['busca'])) {
            $criterio->addCondition('idApartamento', 'like', "%" . strip_tags($_GET['busca']) . "%");
        }

        $view = new View('apartamento', 'all', 'default');
        $view->apartamentos = Apartamento::getList($criterio);
        $view->setTitle("Lista de Apartamentos");
        $view->render();
    }

    public function view() {

        $auth = new Auth();

        if (!$auth->user || $auth->user->getPerfil() == 5) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $id = strip_tags(@$_GET['id']);
        $view = new View('apartamento', 'view', 'default');
        $view->apartamento = new Apartamento($id);
        $view->setTitle("Apartamento");
        $view->render();
    }

    public function edit() {

        $auth = new Auth();

        if (!$auth->user || $auth->user->getPerfil() == 5) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $id = strip_tags(@$_GET['id']);
        $apartamento = new Apartamento($id);
        $view = new View("apartamento", "edit", "default");
        $view->apartamento = $apartamento;
        $view->setTitle("Edição de Apartamento");

        if (count($_POST) > 0) {
            $apartamento->setArApartamento(strip_tags(@$_POST['arApartamento']));
            $apartamento->setTvApartamento(strip_tags(@$_POST['tvApartamento']));
            if ($apartamento->save()) {
                $msg = "Editado com sucesso!";
                $this->all();
            } else {
                $msg = "Erro do Registro!";
            }
        }
        $view->render();
    }

    public function delete() {

        $auth = new Auth();

        if (!$auth->user || $auth->user->getPerfil() == 5) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $id = strip_tags(@$_GET['id']);
        $apartamento = new Apartamento($id);
        $apartamento->delete();

        $this->all();
        return;
    }

}
?>
