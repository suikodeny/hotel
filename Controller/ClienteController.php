<?php

/**
 * Description of ClienteContoller
 *
 * @author Atila Alves Pires
 */
class ClienteController {

    public function all() {

        $auth = new Auth();

        if (!$auth->user) {
            die('acesso negado');
            
        } else if ($auth->user->getPerfil() == 5) {
            $criterio = new Criteria();            
            $criterio->addCondition('idCliente', '=', $auth->user->getIdCliente());
        } else {
            $criterio = new Criteria();
            $criterio->addOrder("nomeCliente");
            $criterio->addCondition('ativo', '=', TRUE);
        }

        if (isset($_GET['busca'])) {
            $criterio->addCondition('nomeCliente', 'like', "%" . strip_tags($_GET['busca']) . "%");
        }

        $view = new View('cliente', 'all', 'default');
        $view->clientes = Cliente::getList($criterio);
        $view->user = $auth->user;
        $view->setTitle("Lista de Clientes");
        $view->render();
    }

    public function view() {

        $auth = new Auth();

        if (!$auth->user || $auth->user->getPerfil() == 5 && $auth->user->getIdCliente() <> strip_tags(@$_GET['id'])) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }
        
        $id = strip_tags(@$_GET['id']);
        $view = new View('cliente', 'view', 'default');
        $view->cliente = new Cliente($id);
        $view->setTitle("Cliente");
        $view->render();
    }

    public function add() {

        $auth = new Auth();

        if (!$auth->user || $auth->user->getPerfil() == 5) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $cliente = new Cliente();
        $login = new Login();
        $view = new View("cliente", "add", "default");
        $view->setTitle("Cadastro de Cliente");

        if (count($_POST) > 0) {

            //criar o cliente
            $cliente->setCpfCliente(strip_tags(@$_POST['cpfCliente']));
            $cliente->setNomeCliente(strip_tags(@$_POST['nomeCliente']));
            $cliente->setSobrenomeCliente(strip_tags(@$_POST['sobrenomeCliente']));
            $cliente->setEmailCliente(strip_tags(@$_POST['emailCliente']));
            $cliente->setSexoCliente(strip_tags(@$_POST['sexoCliente']));
            $cliente->setPerfil(5);

            //ao mesmo tempo criar o usuario
            $login->setUsuarioLogin($cliente->getEmailCliente());
            $login->setSenhaLogin(hash("sha512", "12345678"));
            $login->setTentativas(0);

            if ($cliente->save()) {
                $msg = "Registrado com sucesso! Foi criado o usuário " . $cliente->getEmailCliente() . " para acesso ao sistema";
                new Msg($msg, 1);
                $login->save();
                $this->all();
                return;
            } else {
                $msg = "Erro do Registro!";
            }
        }
        $view->cliente = $cliente;
        $view->render();
    }

    public function edit() {

        $auth = new Auth();

         if (!$auth->user || $auth->user->getPerfil() == 5 && $auth->user->getIdCliente() <> strip_tags(@$_GET['id'])) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $id = strip_tags(@$_GET['id']);
        $cliente = new Cliente($id);
        $login = new Login();
        $view = new View("cliente", "edit", "default");
        $view->cliente = $cliente;
        $view->setTitle("Edição de Cliente");

        if (count($_POST) > 0) {
            $cliente->setNomeCliente(strip_tags(@$_POST['nomeCliente']));
            $cliente->setSobrenomeCliente(strip_tags(@$_POST['sobrenomeCliente']));
            $cliente->setEmailCliente(strip_tags(@$_POST['emailCliente']));
            $cliente->setSexoCliente(strip_tags(@$_POST['sexoCliente']));

            //ao mesmo tempo criar o usuario            
            $login->setUsuarioLogin($cliente->getEmailCliente());

            if ($cliente->save()) {
                $msg = "Editado com sucesso!";
                $login->save();
                $this->all();
                return;
            } else {
                $msg = "Erro do Registro!";
            }
        }
        $view->render();
    }

    public function delete() {

        $auth = new Auth();

         if (!$auth->user || $auth->user->getPerfil() == 5) {
            die('acesso negado');
            ?>
            <META HTTP-EQUIV="REFRESH" CONTENT="3; URL=../?m=login&a=login">
            <?php
        }

        $id = strip_tags(@$_GET['id']);
        $cliente = new Cliente($id);

        $criterio = new Criteria();
        $criterio->addCondition('usuarioLogin', '=', $cliente->getEmailCliente());

        $usuario = Login::getList($criterio);
        $login = new Login(intval($usuario[0]->getIdLogin()));

        $cliente->setAtivo(FALSE);
        $login->setAtivo(FALSE);

        if ($cliente->save()) {
            $login->save();
        }

        $this->all();
        return;
    }

}

?>
