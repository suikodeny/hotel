<?php !isset($this) ? exit : true; ?>

<table class="view"> 

    <?php
    echo "<h1> Apartamento: " . $apartamento->getIdApartamento() . "</h1>";

    if ($apartamento->getReservas() <> NULL) {
        echo "<ul type='square'><h3>Reservas</h3>";
        foreach ($apartamento->getReservas() as $reserva) {
            echo "<li><b>Reserva: </b>" . $reserva->getIdReserva() . "</li>";
            $cliente = New Cliente($reserva->getIdCliente());
            echo "<li><b>Cliente: </b>" . $cliente->getNomeCliente() . "</li>";
            echo "<li><b>Data: </b>" . strftime('%d/%m/%y',$reserva->getDataReserva())  . "</li>";
            echo "<li><a href='?m=reserva&a=delete&id=" . $reserva->getIdReserva() ."'>Delete</a></li>";
            echo '**';
        }
        echo "</ul>";
    } else {
        echo 'Não há reserva';
    }
    ?>

</table>