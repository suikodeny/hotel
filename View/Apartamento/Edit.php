<?php
!isset($this) ? exit : true;
?>
<!-- somente a essencia do que será mostrado -->

<form method="post">
    <fieldset>
        <legend>Edição de Apartamento</legend>
        <div>
            <label for="idApartamento">Apartamento:</label>
            <input type="text" value="<?php echo $apartamento->getIdApartamento(); ?>" name="idApartamento" id="idApartamento" required>
            <br/>
            
            <label class="label_add" for="arApartamento">Ar no apartamento:</label>
            <select name="arApartamento" id="arApartamento" required>
                <?php
                if ($apartamento->getArApartamento() == FALSE) {
                    echo "<option value='0' selected>Não</option>";
                    echo "<option value='1'>Sim</option>";
                } else {
                    echo "<option value='0'>Não</option>";
                    echo "<option value='1' selected>Sim</option>";
                }
                ?>
            </select>
            <br/>
            
            <label class="label_add" for="tvApartamento">TV no apartamento:</label>
            <select name="tvApartamento" id="tvApartamento" required>
                <?php
                if ($apartamento->getTvApartamento() == FALSE) {
                    echo "<option value='0' selected>Não</option>";
                    echo "<option value='1'>Sim</option>";
                } else {
                    echo "<option value='0'>Não</option>";
                    echo "<option value='1' selected>Sim</option>";
                }
                ?>
            </select>
        </div>
        <input class="botao_submit" type="submit" value="Salvar">
    </fieldset>
</form>