<?php
!isset($this) ? exit : true;
?>
<!-- somente a essencia do que será mostrado -->

<header id="header_section">
    <form action="#" method="get">
        <input type="text" id="busca" name="busca" placeholder="Pesquisar por nome">
        <input type="hidden" name="m" id="m" value="apartamento">
        <input type="hidden" name="a" id="a" value="all">
        <input type="submit" value="Buscar">
    </form>
</header>

<table class="tabela_all">    
    
<?php
echo "<tr>";
echo "<th class='cabecalho_table'>Apartamento</th>";
echo "<th class='cabecalho_table' colspan='2'>Opções</th>";
echo "</tr>";
foreach ($apartamentos as $apartamento) {
    echo "<tr>";
    echo "<td class='linha_table'> Número " .  $apartamento->getIdApartamento() ."</td>";    
    echo "<td class='linha_table_opcoes'><a href='?m=apartamento&a=view&id=" . $apartamento->getIdApartamento() ."'>Ver</a></td>";
    echo "<td class='linha_table_opcoes'><a href='?m=apartamento&a=edit&id=" . $apartamento->getIdApartamento() ."'>Editar</a></td>";
    
    echo "</tr>";
}

?>

</table>