<?php
!isset($this) ? exit : true;
?>
<!-- somente a essencia do que será mostrado -->

<form method="post">
    <fieldset>
        <legend>Edição de Usuario</legend>
        <div>
            <label for="usurioLogin">Usuário:</label>
            <input type="text" value="<?php echo $user->getUsuarioLogin(); ?>" name="usuarioLogin" id="usuarioLogin" required>
            <br/>

            <label for="senha_usuario">Senha:</label>
            <input type="text" value="" name="senha_usuario" id="senha_usuario">
            <br/>

            <label for="repete_senha_usuario">Repete senha:</label>
            <input type="text" value="" name="repete_senha_usuario" id="repete_senha_usuario">
            <br/>

            <label for="codPerfil">Perfil:</label>
            <select id="codPerfil" name="codPerfil">
                <?php
                $criterio = new Criteria();
                $criterio->addCondition("emailCliente", "=", $user->getUsuarioLogin());

                $lista = Cliente::getList($criterio);
                $cliente = new Cliente(intval($lista[0]->getIdCliente()));

                if ($cliente->getPerfil() == 5) {
                    echo "<option value='5' selected>Comum</option>";
                    echo "<option value='1'>Admin</option>";
                } else {
                    echo "<option value='5'>Comum</option>";
                    echo "<option value='1' selected>Admin</option>";
                }
                ?>                
            </select>

        </div>
        <input class="botao_submit" type="submit" value="Salvar">
    </fieldset>
</form>