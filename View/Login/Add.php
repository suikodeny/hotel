<?php
!isset($this) ? exit : true;
?>
<!-- somente a essencia do que será mostrado -->

<form method="post">
    <fieldset>
        <legend>Cadastro de Usuário</legend>
        <div>
            <label for="nome_usuario">Nome:</label>
            <input type="text" name="nome_usuario" id="nome_usuario" placeholder="Primeiro Nome" required>
            <br/>
            <label for="sobrenome_usuario">Sobrenome:</label>
            <input type="text" name="sobrenome_usuario" id="sobrenome_usuario" placeholder="Sobrenome" required>
            <br/>
            <label for="email_usuario">Email:</label>
            <input type="text" name="email_usuario" id="email_usuario" placeholder="Email" required>
            <br/>
            <label for="login_usuario">Login:</label>
            <input type="text" name="login_usuario" id="login_usuario" placeholder="Login" required>
            <br/>
            <label for="senha_usuario">Senha:</label>
            <input type="text" name="senha_usuario" id="senha_usuario" placeholder="Senha" required>
            <br/>
            <label for="repete_senha_usuario">Repita a Senha:</label>
            <input type="text" name="repete_senha_usuario" id="repete_senha_usuario" placeholder="Repita a Senha" required>
        </div>
        <input class="botao_submit" type="submit" value="Salvar">
    </fieldset>
</form>