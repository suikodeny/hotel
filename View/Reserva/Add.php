<?php
!isset($this) ? exit : true;
?>
<!-- somente a essencia do que será mostrado -->

<form method="post">
    <fieldset>
        <legend>Cadastro de Reserva</legend>
        <div>
            <label for="dataReserva">Data:</label>
            <input type="date" name="dataReserva" id="dataReserva" placeholder="ex: dd/mm/aa" required>

            <label for="idApartamento">Apartamento:</label>
            <select name="idApartamento" id="idApartamento">
                <?php
                $criterio = new Criteria();
                $criterio->addOrder("idApartamento");
                $criterio->addCondition('reservadoApartamento', '=', FALSE);
                foreach (Apartamento::getList($criterio) as $valor) {
                    echo "<option value='" . $valor->getIdApartamento() . "'>" . $valor->getIdApartamento() . "</option>";
                }
                ?>
            </select>
            <label for="idCliente">Cliente:</label>
            <select name="idCliente" id="idCliente">
                <?php
                $criterio = new Criteria();
                if ($user->getPerfil() == 5) {
                    $criterio->addCondition("idCliente", "=", $user->getIdCliente());
                } else {
                    $criterio->addOrder("nomeCliente");
                    $criterio->addCondition("ativo", "=", TRUE);
                }
                foreach (Cliente::getList($criterio) as $valor) {
                    echo "<option value='" . $valor->getIdCliente() . "'>" . $valor->getNomeCliente() . "</option>";
                }
                ?>
            </select>
        </div>
        <input class="botao_submit" type="submit" value="Salvar">
    </fieldset>
</form>