<?php
!isset($this) ? exit : true;
?>
<!-- somente a essencia do que será mostrado -->

<form method="post">
    <fieldset>
        <legend>Edição de Reserva</legend>
        <div>
            <label for="nomeReserva">Reserva:</label>
            <input type="text" value="<?php echo $reserva->getNomeExer(); ?>" name="nomeReserva" id="nomeReserva" required>
            <select name="reserva" id="reserva">
                <?php
                $criterio = new Criteria();
                $criterio->addOrder("nomeCategoria");
                foreach (Categoria::getList($criterio) as $valor) {
                    if($reserva->setIdCategoriaExer() == $valor->getIdCategoria()){
                        echo "<option value='" . $valor->getIdCategoria() . "' selected>" . $valor->getNomeCategoria() . "</option>";
                    }else{
                        echo "<option value='" . $valor->getIdCategoria() . "'>" . $valor->getNomeCategoria() . "</option>";
                    }                    
                }
                ?>
            </select>
        </div>
        <input class="botao_submit" type="submit" value="Salvar">
    </fieldset>
</form>