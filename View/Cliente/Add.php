<?php
!isset($this) ? exit : true;
?>
<!-- somente a essencia do que será mostrado -->

<form method="post">
    <fieldset>
        <legend>Cadastro de Cliente</legend>    

        <label class="label_add" for="cpfCliente">CPF:</label>
        <input type="cpf" name="cpfCliente" id="cpflCliente" size="12" placeholder="CPF sem pontuação" required>
        <br/>

        <label class="label_add" for="nomeCliente">Nome:</label>
        <input type="text" name="nomeCliente" id="nomeCliente" size="40" placeholder="Nome" required>
        <br/>
        <label class="label_add" for="sobrenomeCliente">Sobrenome:</label>
        <input type="text" name="sobrenomeCliente" id="sobrenomeCliente" size="60" placeholder="Sobrenome" required>
        <br/>

        <label class="label_add" for="emailCliente">Email:</label>
        <input type="email" name="emailCliente" id="emailCliente" size="60" placeholder="Email" required>
        <br/>

        <label class="label_add" for="sexoCliente">Sexo:</label>
        <select name="sexoCliente" id="sexoCliente" required>
            <option value="M">Masculino</option>
            <option value="F">Feminino</option>
        </select> 
        <br/>            

        <input class="botao_submit" type="submit" value="Salvar">
    </fieldset>
</form>