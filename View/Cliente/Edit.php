<?php
!isset($this) ? exit : true;
?>
<!-- somente a essencia do que será mostrado -->

<form method="post">
    <fieldset>
        <legend>Edição de Cliente</legend>
        <div>
            <label class='label_add' for='cpfCliente'>CPF:</label><?php echo $cliente->getCpfCliente(); ?>
            <br/>
            <label class="label_add" for="nomeCliente">Nome:</label>
            <input type="text" value="<?php echo $cliente->getNomeCliente(); ?>" name="nomeCliente" id="nomeCliente" size="40" required>
            <br/>
            <label class="label_add" for="sobrenomeCliente">Sobrenome:</label>
            <input type="text" value="<?php echo $cliente->getSobrenomeCliente(); ?>" name="sobrenomeCliente" id="sobrenomeCliente" size="60" placeholder="Sobrenome" required>
            <br/>

            <label class="label_add" for="emailCliente">Email:</label>
            <input type="text" value="<?php echo $cliente->getEmailCliente(); ?>" name="emailCliente" id="emailCliente" size="60" placeholder="Email" required>
            <label class="label_add" for="sexoCliente">Sexo:</label>

            <select name="sexoCliente" id="sexoCliente" required>
                <?php
                if ($cliente->getSexoCliente() == "M") {
                    echo "<option value='M' selected>Masculino</option>";
                    echo "<option value='F'>Feminino</option>";
                } else {
                    echo "<option value='M'>Masculino</option>";
                    echo "<option value='F' selected>Feminino</option>";
                }
                ?>
            </select>
            <br/>

        </div>
        <input class="botao_submit" type="submit" value="Salvar">
    </fieldset>
</form>