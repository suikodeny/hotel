<?php

!isset($this) ? exit : true;

echo "<fieldset id='fieldset_view'>";
echo "<legend>Usuário: " . $cliente->getIdCliente() . "</legend>";
echo "<p>";
echo "<label class='label_view' for='cpfCliente'>CPF:</label>" . $cliente->getCpfCliente();
echo "<br/>";
echo "<label class='label_view' for='nomeCliente'>Nome:</label>" . $cliente->getNomeCliente();
echo "<label class='label_view' for='sobrenomeCliente'>Sobrenome:</label>" . $cliente->getSobrenomeCliente();
echo "<label class='label_view' for='emailCliente'>Email:</label>" . $cliente->getEmailCliente();
echo "<label class='label_view' for='sexoCliente'>Sexo:</label>" . $cliente->getSexoCliente();
echo "<br/>";
echo "</p>";


echo "<table class='view'>";

if ($cliente->getReservas() <> NULL) {
    echo "<ul type='square'><h3>Reservas</h3>";
    foreach ($cliente->getReservas() as $reserva) {
        echo "<li><b>Reserva: </b>" . $reserva->getIdReserva() . "</li>";
        echo "<li><b>Data: </b>" . strftime('%d/%m/%y', $reserva->getDataReserva()) . "</li>";
        echo '**';
    }
    echo "</ul>";
} else {
    echo 'Não possui reserva';
}
echo "</table>";

echo '</fieldset>';

