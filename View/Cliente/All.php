<?php
!isset($this) ? exit : true;
?>
<!-- somente a essencia do que será mostrado -->

<header id="header_section">
    <form action="#" method="get">
        <input type="text" id="busca" name="busca" placeholder="Pesquisar por nome">
        <input type="hidden" name="m" id="m" value="cliente">
        <input type="hidden" name="a" id="a" value="all">
        <input type="submit" value="Buscar">
    </form>
</header>


<table  class="tabela_all">    

    <?php
    echo "<tr>";
    echo "<th class='cabecalho_table'>Clientes</th>";
    echo "<th class='cabecalho_table' colspan='3'>Opções</th>";
    echo "</tr>";
    foreach ($clientes as $cliente) {
        echo "<tr>";
        echo "<td class='linha_table'>" . $cliente->getIdCliente() . " - " . $cliente->getNomeCliente() . " " . $cliente->getSobrenomeCliente() . "</td>";
        echo "<td class='linha_table_opcoes'><a href='?m=cliente&a=view&id=" . $cliente->getIdCliente() . "'>Ver</a></td>";
        echo "<td class='linha_table_opcoes'><a href='?m=cliente&a=edit&id=" . $cliente->getIdCliente() . "'>Editar</a></td>";
        if ($user->getIdCliente() == 1) {
            echo "<td class='linha_table_opcoes'><a href='?m=cliente&a=delete&id=" . $cliente->getIdCliente() . "'>Delete</a></td>";
        }

        echo "</tr>";
    }
    ?>

</table>